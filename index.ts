import getMessageFromTemplate from './src/getMessageFromTemplate';

const template = {
    if: '(mark >= 4)',
    then: 'Good work!',
    else: {
      if: '(lesson === "english")',
      then: 'Learn grammar',
      else: 'You needed more training',
    },
};


console.log(getMessageFromTemplate({mark: 3, lesson: 'any'}, template));
console.log(getMessageFromTemplate({mark: 3, lesson: 'english'}, template));
console.log(getMessageFromTemplate({mark: 4, lesson: 'english'}, template));