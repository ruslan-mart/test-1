export type Config = {
    strictMode?: boolean;
    throwError?: boolean;
}

export type ExpressionHashMap = {
    expression: string;
    map: {
        [hash: string]: string;
    };
};

export type Vars = {
    [varName: string]: any;
}


export interface Template {
    if: string;
    then: string | Template;
    else?: string | Template;
}