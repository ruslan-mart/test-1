import getMessageFromTemplate from './getMessageFromTemplate';


test('test 1', () => {
    const vars = {
        foo: 'bar',
    };

    const template = {
        if: 'foo === "bar"',
        then: 'positive',
        else: 'negative'
    };

    expect(getMessageFromTemplate(vars, template)).toBe('positive');
});


test('test 2', () => {
    const vars = {
        foo: 'bar',
    };

    const template = {
        if: 'foo !== "bar"',
        then: 'positive',
        else: 'negative'
    };

    expect(getMessageFromTemplate(vars, template)).toBe('negative');
});


test('test 3', () => {
    const vars = {
        foo: 1,
        bar: 1,
    };

    const template = {
        if: 'foo === bar',
        then: {
            if: 'foo === 1 && bar === 1',
            then: 'positive'
        },
        else: 'negative'
    };

    expect(getMessageFromTemplate(vars, template)).toBe('positive');
});


test('test 4', () => {
    const vars = {
        foo: 1,
        bar: 1,
        baz: 2,
    };

    const template = {
        if: 'foo + bar === 3',
        then: 'positive',
        else: {
            if: 'baz === 3',
            then: 'positive',
            else: 'negative'
        }
    };

    expect(getMessageFromTemplate(vars, template)).toBe('negative');
});


test('test 5', () => {
    const vars = {
        color: 'red',
        size: 5,
    };

    const template = {
        if: 'color === "blue" || color === "red"',
        then: {
            if: 'size > 1',
            then: 'positive'
        },
        else: 'negative'
    };

    expect(getMessageFromTemplate(vars, template)).toBe('positive');
});


test('test 6', () => {
    const vars = {
        color: 'green',
        size: 10,
    };

    const template = {
        if: 'color === "blue" || color === "red"',
        then: {
            if: 'size > 1',
            then: 'positive'
        },
        else: {
            if: 'color === "green" && (size === 0 || size === 10 || size === 20) || color === "pink"',
            then: 'positive-2',
            else: 'negative-2'
        }
    };

    expect(getMessageFromTemplate(vars, template)).toBe('positive-2');
});


test('test 7', () => {
    const vars = {
        a: 1,
        b: 2,
        c: 3,
        d: 4,
        e: 5,
    };

    const template = {
        if: 'a >= 0 && a <= 10',
        then: {
            if: 'b + 8 === 10',
            then: {
                if: 'c === d - 1',
                then: {
                    if: 'e === 25/5',
                    then: 'positive'
                }
            }
        },
        else: 'negative'
    };

    expect(getMessageFromTemplate(vars, template)).toBe('positive');
});


test('test 8', () => {
    const vars = {
        a: 1,
        b: 2,
        c: 3,
        d: 4,
        e: 5,
    };

    const template = {
        if: 'a >= 0 && a <= 10',
        then: {
            if: 'b + 8 === 10',
            then: {
                if: 'c === d - 1',
                then: {
                    if: 'e === 35/5',
                    then: 'positive'
                }
            }
        },
        else: 'negative'
    };

    expect(getMessageFromTemplate(vars, template)).toBe(null);
});