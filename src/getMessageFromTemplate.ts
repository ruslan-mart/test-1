import * as Type from './getMessageFromTemplate.typed';


const DEFAULT_STRICT_MODE = true;
const DEFAULT_THROW_ERROR = true;


const expressionPatternDelimiter = /\s*(?:&&)|(?:\|\|)\s*/;
const expressionPatternOperators = /\s*(?:[-+*\/%<>])|(?:[<>=!]=)\s*/;
const expressionPatternValue = /^\s*(?:(?:[+-?]?\d+(?:.\d+)?)|(?:(?:@@)?\w+))\s*$/;

const validatePropsOrder = {
    'if': 0,
    'then': 1,
    'else': 2,
};


// Случайный хеш
const getHash = (): string => Math.random().toString(36).substr(2);

// Вырезает лишние скобки
const expressionCutBrackets = (expression: string): string => {
    return expression.replace(/^\((.+)\)$/, '$1');
}

// Заменяем значения в "expression" по "pattern" и заменяем сгенерированеым хешем
// Создаём мапу из хешей формата "@@hash = value"
const expressionHashMap = (pattern: RegExp, expression: string): Type.ExpressionHashMap => {
    const map = {};

    const replacer = (matches: string) => {
        const hash = getHash();
        map[hash] = matches;
        return `@@${hash}`;
    };

    while (pattern.test(expression)) {
        expression = expression.replace(pattern, replacer);
    }
    
    return {map, expression};
}

// Функция для тестирования/валидации выражения
const expressionTest = (expression: string) => {
    // Разбиваем выражение на блоки, по "логическим операторам"...
    const expressionBlocks = expression.split(expressionPatternDelimiter);

    // и пробегаемся по ним
    for (let block of expressionBlocks) {
        // Разбиваем блок по арифметическим операторам и операторам сравнения...
        const expressionValues = block.split(expressionPatternOperators);

        // и пробегаемся по значениям
        for (let value of expressionValues) {

            // Валидируем каждое значение
            if (!expressionPatternValue.test(value)) {
                return false;
            }
        }
    }
    
    return true;
}



class Handler {

    private config: Type.Config;
    private template: Type.Template;
    private vars: Type.Vars;


    constructor(vars: Type.Vars, template: Type.Template, { strictMode = DEFAULT_STRICT_MODE, throwError = DEFAULT_THROW_ERROR }: Type.Config = {}) {
        
        if (vars === null || typeof vars !== 'object') {
            this.error(`vars должен быть объектом`);
        }
        
        this.config = {strictMode, throwError};
        this.vars = vars;
        this.template = template;
    }


    // Запускает процесс обработки всего дерева
    public init() {
        return this.handleTemplate(this.template);
    }


    // Вывод ошибки
    private error(message: string): void {
        if (this.config.throwError) {
            throw new Error(message);
        }
        else {
            console.error(message);
        }
    }

    // Выполняет выражение, если оно валидно
    private execExpression(expression): boolean | null {
        if (!this.validateExpression(expression)) {
            this.execExpressionError(expression);
            return null;
        }
        
        // Преобразовываем this.vars в eval код
        // Объявляем переменные как локальные внутри eval-функции
        const evalVars = [];

        for (let varName in this.vars) {
            if (this.vars.hasOwnProperty(varName)) {

                const value = this.vars[varName];

                evalVars.push(`${varName}=${isFinite(value) ? value : `"${value}"`}`);
            }
        }

        // Добавляем в начало undefined, чтобы не было ошибки, если вдруг наш массив evalVars пустой
        // Будет "var undefined;", а это не ошибка :)
        evalVars.unshift('undefined');
        
        // Eval-функция, которая объявит переменные у себя локально и выполнит выражение
        const executer = Function(`
            var ${evalVars.join(',')};
            return ${expression};
        `)

        try {
            // Вызываем нашу eval-функцию и приобразовываем результат в булево
            return Boolean(executer()); // !!executer();
        }
        catch (_e) {
            this.execExpressionError(expression);
            return null;
        }        
    }

    private execExpressionError(expression: string): void {
        this.error(`В выражении что-то не так с синтаксисом: "${expression}"`);
    }

    // Обработчик template
    private handleTemplate(template: Type.Template): string | null {
        // Если template валиден, то...
        if (this.validateTemplate(template)) {
            // вызываем код "условия"
            const conditionValue = this.execExpression(template['if']);

            // Если null, значит что-то не так с синтаксисом в выражении
            if (conditionValue === null) {
                return null;
            }

            // Если "условие" истинное, то возвращаем "then", а если нет - то "else"
            const subtemplate = conditionValue ? template['then'] : template['else'];

            // Проверяем на undefined, потому что "else" может не существовать
            if (subtemplate !== undefined) {
                // Если строка, то возвращаем значение :)
                if (typeof subtemplate === 'string') {
                    return subtemplate;
                }

                // а если нет, то рекурсивно вызываем дальше :)
                return this.handleTemplate(subtemplate);
            }
        }

        return null;
    }


    // Проверка на валидность exression
    private validateExpression(expression: string): boolean {
        const map = {
            screening: null,
            strings: null,
            brackets: null,
        };

        expression = expressionCutBrackets(expression);
    
        // Хэшируем обратные слэши
        ({ map: map.screening, expression } = expressionHashMap(/\\./, expression));

        // Хэшируем строки
        ({ map: map.strings, expression } = expressionHashMap(/(?:"[^"]*?")|(?:'[^']*?')/, expression));

        // Хэшируем скобки
        ({ map: map.brackets, expression } = expressionHashMap(/\([^()]*?\)/, expression));
        
        
        // Проверяем текущее выражение на валидацию...
        if (expressionTest(expression)) {

            // и всё что в "скобках"
            const { brackets } = map;
            
            for (let hash in brackets) {
                if (brackets.hasOwnProperty(hash) && !expressionTest(expressionCutBrackets(brackets[hash]))) {
                    return false;
                }
            }
        }

        return true;
    }


    // Проверка на валидность Template
    private validateTemplate(template: Type.Template): boolean {
        // Если строка или число, то всё ок
        if (typeof template === 'string') {
            return true;
        }

        // Если объект...
        if (template !== null && typeof template === 'object') {

            // то проверяем наличие важных ключей
            // наличие "else" не проверяем, потому что он не обязателен
            if (template.hasOwnProperty('if') && template.hasOwnProperty('then')) {
                
                // Если строгий режим, то ещё и проверяем правильный порядок
                // и чтобы не было никаких других "левых свойств"
                if (this.config.strictMode) {
                    const keys = Object.keys(template);

                    for (let index = 0; index !== keys.length; index++) {
                        const key = keys[index];

                        if (!validatePropsOrder.hasOwnProperty(key) || validatePropsOrder[key] !== index) {
                            this.error(`Ну и что за порядок странный? "${keys.join(', ')}"`);
                            return false;
                        }
                    }
                }

                return true;
            }
            
            this.error(`Обязательные "if" и "then" - отсутствуют!`);
            return false;
        }

        // Иначе "шляпа"
        this.error(`Что-то какой-то левый тип пришёл в template: "${typeof template}"`);
        return false;
    }
}


export default (vars: Type.Vars, template: Type.Template, config?: Type.Config) => new Handler(vars, template, config).init();